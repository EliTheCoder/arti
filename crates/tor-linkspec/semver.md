MODIFIED: New ByRelayIds type.
BREAKING: Changed the semantics of HasAddrs
BREAKING: ChanTarget now requires HasChanMethod
